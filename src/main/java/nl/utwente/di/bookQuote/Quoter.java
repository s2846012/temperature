package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    public Double calculator(String temparture) {
        Double celsius = Double.parseDouble(temparture);
        return (celsius * 1.8) + 32;
    }

}
